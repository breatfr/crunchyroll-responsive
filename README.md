# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>
[Crunchyroll](https://crunchyroll.com/) website is more suitable for wide screens. Works for all languages.
## Preview
![Preview](https://gitlab.com/breatfr/crunchyroll/-/raw/main/docs/preview-series.jpg)

## More previews:
- https://gitlab.com/breatfr/crunchyroll/-/raw/main/docs/preview-notifications.jpg
- https://gitlab.com/breatfr/crunchyroll/-/raw/main/docs/preview-videos.jpg
- https://gitlab.com/breatfr/crunchyroll/-/raw/main/docs/preview-watchlist.jpg

## How to use in few steps
1. Install Stylus browser extension
    - Chromium based browsers link: https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne
        - Brave
        - Chromium
        - Google Chrome
        - Iridium Browser
        - Microsoft Edge
        - Opera
        - Opera GX
        - SRWare Iron
        - Ungoogled Chromium
        - Vivaldi
        - Yandex Browser
        - many more
    - Firefox based browsers link: https://addons.mozilla.org/firefox/addon/styl-us/
        - Mozilla Firefox
        - Mullvad Browser
        - Tor Browser
        - Waterfox
        - many more

2. Go on [UserStyles.world](https://userstyles.world/style/16587) website and click on `Install` under the preview picture or open the [GitLab version](https://gitlab.com/breatfr/crunchyroll/-/raw/main/css/crunchyroll-responsive.user.css).

3. To update the theme, open the `Stylus Management` window and click on `Check for update` and follow the instructions or just wait 24h to automatic update

4. Enjoy :)
# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>